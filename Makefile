CVERS=4.7.12
REG_HOST=registry.gitlab.inria.fr
REG_IMAGE=capsid-public/conda/runner-build
IMAGE=$(REG_HOST)/$(REG_IMAGE)

build:
#registry.gitlab.inria.fr/capsid/conda/runner-build
	docker build -t $(IMAGE):$(CVERS) --build-arg cvers=$(CVERS) .
	docker tag $(IMAGE):$(CVERS) $(IMAGE):latest

push:
	docker push $(IMAGE):$(CVERS)
	docker push $(IMAGE):latest
