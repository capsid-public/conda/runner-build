ARG cvers
FROM continuumio/miniconda3:$cvers

RUN conda config --add channels conda-forge && conda install -y conda-build
